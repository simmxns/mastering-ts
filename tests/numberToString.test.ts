import { numberToString } from '../src/specials/wars/numberToString'

describe("Passing a number and returning a string", () => {
  test('toBe', () => {
    expect(numberToString(67)).toBe('67')
    expect(numberToString(100.59)).toBe('100.59')
    expect(numberToString(-3.14)).toBe('-3.14')
  }) 
})
