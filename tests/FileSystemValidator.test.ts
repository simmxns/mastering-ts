import FileSystemValidator from '../src/validators/FileSystemValidator'

describe('validate if a string is a path like', () => {
	test('truthines', () => {
		const fv = new FileSystemValidator()

		expect(fv.fileExists('')).toBeFalsy()
		expect(fv.fileExists('~/workspace/index.html')).toBeTruthy()
	})
})