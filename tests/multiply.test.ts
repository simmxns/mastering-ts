import * as x from '../src/specials/wars/multiply'

describe("Multiplying in different ways", () => {
	test("for loop / toBe", () => {
		expect(x.forMultiply(50, -50)).toBe(-2500)
	})

	test("while loop / toBe", () => {
		expect(x.whileMultiply(50, 50)).toBe(2500)
	})

	test("string / toBe", () => {
		expect(x.stringMultiply(2, 4)).toBe(8)
	})

	test("recursive function / toBe", () => {
		expect(x.recursiveMultiply(2, 100)).toBe(200)
	})

	test("division / toBe", () => {
		expect(x.divideMultiply(5, -5)).toBe(-25)
	})
})
