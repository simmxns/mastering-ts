const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CheckerPlugin } = require('awesome-typescript-loader');

const config = {
  entry: [
    'react-hot-loader/patch',
    './src/index.tsx'
	],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    plugins: [
      new TsconfigPathsPlugin({
        configFile: path.resolve(__dirname, './tsconfig.json'),
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        logLevel: 'INFO',
        baseUrl: path.resolve(__dirname, './src/')
      }),
    ]
  },
  mode: 'development',
  devtool: 'source-map',
  watch: true,
	devServer: {
		static: {
			directory: path.join(__dirname, 'public')
		},
		compress: true,
		port: 9000
	},
  optimization: {
    minimize: true,
    minimizer: [ new TerserPlugin() ],
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/,
        options: {
          plugins: ['lodash'],
          presets: [['env', { 'modules': false, 'targets': { 'node': 4 } }]]
        }
      },
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      },
			{
				test: /\.css$/i,
        use: [ MiniCssExtractPlugin.loader, "css-loader" ],
			}
    ]
  },
  plugins: [
    new LodashModuleReplacementPlugin({
      collections: true,
      paths: true
    }),
    new CheckerPlugin(),
		new MiniCssExtractPlugin()
  ]
};

module.exports = config;
