import type { DOGS_ACTIONS } from './types'

export default (
	state: {
		dogs: []
		liked: false
	},
	action: DOGS_ACTIONS
) => {
	const { payload, type } = action

	const dogActions: any = {
		'GET_DOGS': {
			...state,
			dogs: payload
		},
		'PUSH_LIKE': {
			...state,
			liked: payload
		}
	}

	return dogActions[type] ?? state
}
