import { rotateOnDown, rotateOnUp } from '@/helpers/rotateCard'
import React, { useEffect, useRef, useContext } from 'react'
import DogContext from '../context/Dog/DogContext'
import { Heart } from '../components/icons/Heart'
import { useCounter } from '../hooks/useCounter'

export default function DogsCard(): JSX.Element {
	const card = useRef<HTMLDivElement>(null)
	const { getDogs, giveLike, dogs, liked } = useContext(DogContext)
	const { counter, decrement, increment } = useCounter(0)

	useEffect(() => {
		getDogs?.()
	}, [])
	
	const rotateCardActive = (e: React.MouseEvent): void => {
		const { current: element } = card
		rotateOnDown<HTMLDivElement>(e, element!, {
			classOne: 'rotateUp',
			classTwo: 'rotateDown',
			matcherA: 'a',
			matcherB: 'b'
		})
	}
	const rotateCardInactive = (e: React.MouseEvent): void => {
		const { current: element } = card
		rotateOnUp<HTMLDivElement>(e, element!, {
			classOne: 'rotateUp',
			classTwo: 'rotateDown',
			matcherA: 'a',
			matcherB: 'b'
		})
	}

	const onClickUpHandler = () => increment({ limit: dogs.length - 1, value: 0 })
	const onClickDownHandler = () => decrement({ limit: 0, value: dogs.length - 1 })
	const onLikeHandler = () => giveLike?.()

	return (
		<div
			className={`card dogs${liked ? ' liked' : ''}`}
			style={{ backgroundImage: 'url("' + dogs[counter] + '")' }}
			ref={card}
		>
			<button
				onClick={onClickUpHandler}
				onMouseDown={rotateCardActive}
				onMouseUp={rotateCardInactive}
				className="btn-card a"
			/>
			<Heart onClick={onLikeHandler} />
			<button
				onMouseDown={rotateCardActive}
				onMouseUp={rotateCardInactive}
				onClick={onClickDownHandler}
				className="btn-card b"
			/>
		</div>
	)
}
