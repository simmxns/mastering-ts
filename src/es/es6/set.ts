/**
 * Set
 * 
 * Allow to store unique values of any data type.
 * returns a new instance of Set.
 * empty or null iterable returns empty Set.
 * 
 * new Set([iterable])
 */
export const set: Set<string | number> = new Set([20, 20, 10, 5, 5, 21, 54, 'a', 'a', 'a', 'b', '30', 30])

set.has('30') // true
set.delete('a') // true
set.add(20) // Set(8) { 20, 10, 5, 21, 54, 'b', '30', 30 }

// set to array how?
//export const setToArray = [...set]
export const setArray = Array.from(set)
set.clear()
