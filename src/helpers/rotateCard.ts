type rotateClass = {
	classOne: string
	classTwo: string
	matcherA: string
	matcherB: string
}

/**
 * It Takes two classes and two matchers if are truthy the classes will be added in the element
 * 
 * @param e Event
 * @param element Element to be alter
 * @param params Parameteres to iteractive with the element
 */
export function rotateOnDown<T extends Element>(
	e: any,
	element: T,
	params: rotateClass
): void {
	const { classOne, classTwo, matcherA, matcherB } = params

	if (e.target.classList.contains(matcherA))
		element.classList.add(classOne)
	if (e.target.classList.contains(matcherB))
		element.classList.add(classTwo)
}

/**
 * It Takes two classes and two matchers if are truthy the classes will be removed in the element
 * 
 * @param e Event
 * @param element Element to be alter
 * @param params Parameteres to iteractive with the element
 */
export function rotateOnUp<T extends Element>(
	e: any,
	element: T,
	params: rotateClass
): void {
	const { classOne, classTwo, matcherA, matcherB } = params

	if (e.target.classList.contains(matcherA))
		element.classList.remove(classOne)
	if (e.target.classList.contains(matcherB))
		element.classList.remove(classTwo)
}