#!/usr/local/bin/node
import { CONSOLE_CSS } from '@/helpers/console-css'
import CountCard from '@/specials/tsx/components/CountCard'
import React, { useEffect, StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { hot } from 'react-hot-loader/root'
import DogState from './specials/tsx/context/Dog/DogState'
import DogsCard from './specials/tsx/components/DogCard'

function App(): JSX.Element {
	// use effect runs when the component render completely
	useEffect(() => {
		console.log('%cComponent loaded!', CONSOLE_CSS)
	})

	return (
		<>
			<StrictMode>
				<CountCard />
				<DogState>
					<DogsCard />
				</DogState>
			</StrictMode>
		</>
	)
}

ReactDOM.render(<App />, document.querySelector('#root'))

export default hot(App)
