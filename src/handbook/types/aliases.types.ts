import { User, Admin } from './intersection.types'

export type numberOrString = number | string
export type UserAdmin = User & Admin
export type funcString = () => string
export type NonNullArray = {
	value: number
}[]

declare function executor(f: funcString): void
executor(() => "string")

