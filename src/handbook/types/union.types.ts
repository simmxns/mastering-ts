/* union types quitan algunos de los beneficios del tipado
	 estatico nos quitan la seguridad de saber que tipo es un objeto
	 y que operaciones se pueden hacer con este
*/

export let age: number | string | boolean;
