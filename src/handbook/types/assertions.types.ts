import { User, Admin } from './intersection.types'
/*
	Se confunden casting (cambiar momentaneamente el tipo de dato)
	No cambia el objeto cambia el tipo de dato que el compilador les habia otorgado
	Corrige el dato que ts le habia asignado a un objeto
*/

let user: User & Admin
user = new User() as User & Admin

declare interface AJAXSettings {
	url: string
}

const options = {} as AJAXSettings
options.url = 'https://poetrydb.org/title/Ozymandias/lines.json'