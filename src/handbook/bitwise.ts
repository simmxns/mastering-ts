import { DocPermissions as DP } from './types/permissions.types'
import type { Bitwise } from './types/bitwise.types'
/*
 Bitwise operators
 works in individual bits of a number
 NOTE: bitwise operators coverts the operand to a 32 bits signed interger

 1	 			= 00000001
 2	 			= 00000010
 -          --------
 OR	 			= 00000011 # returns 1 when one or both are 1s
 AND 			= 00000000 # returns 1 when both operand are 1s
 XOR 			= 00000011 # returns 1 when some operand have 1s but not both
 NOT 			= 11111101 # 0s to 1s and vice versa, negative operands will be converted to positive and vice versa
 LSHIFT 	= 00000000 # 0s added at the left many times as right number says (shift by 1 is the same to divide by 2)
 RSHIFT 	= 00000100 # 0s added at the right many times as right number says
 URSHIFT  = # 0s added at the right many times as right number says
*/
export default <Bitwise> {
	"OR": 1 | 2,
	"AND": 1 & 2,
	"XOR": 1 ^ 2,
	"NOT": ~-1,
	"LSHIFT": 1 << 2,
	"RSHIFT": 1 >> 2,
	"NRSHIFT": -1 >> 2,
	"URSHIFT": 1 >>> 2,
	"NURSHIFT": -1 >>> 2
}

// one OR/AND real use case
let myPermission = 0
myPermission = myPermission | DP.comment | DP.edit | DP.view
export let message = myPermission & DP.comment ? 'yes' : 'no'