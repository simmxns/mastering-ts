export const getDeepValue = <
	T,
	TKey extends keyof T,
	TSubKey extends keyof T[TKey]
>(
	obj: T,
	firstKey: TKey,
	secondKey?: TSubKey
) => obj[firstKey][secondKey!]

const obj = {
	foo: {
		a: 18,
		b: true,
		c: [1, 2, 3]
	},
	bar: {
		d: -Infinity,
		e: 'Potato',
		f: () => console.log(`hi world!`)
	}
}

getDeepValue(obj, 'foo', 'c')