class Requestor {
	// Propiedad estatica, ya no pertece a this sino a la clase
	static url: string = "https://codigofacilito.com/videos"

	/* Al ser estatica solo se creara una copia de la variable
	 * 1. La informacion le pertenece a la clase y no al objeto
	 * 2. Solo se necesita una copia de dicha variable y no mas porque no va a ser modificada entre cada uno de los objetos
	 * 3. Se accede a esta variable usando la clase en lugar de la palabra reservada "this"
	 */

	static getCourses() {
		console.log(`${Requestor.url}/cursos`)
	}
	getArticles() {
		console.log(`${Requestor.url}/articles`)
	}
}

/* Estos se usan cuando los metodos no necesitan de un estado interno
 * Cada objeto tiene un objeto, este representa los valores de cada atributo del objeto que pudeen ser diferentes entre cada uno de los objetos, por ello cada uno de los objetos conserva su propio estado
 * Cuando no necesitamos valores propios para llamar a cada objeto se utilizan los metodos estaticos
 */
Requestor.getCourses();

export default Requestor
