/**
 * Property Decorators
 * 
 * recibe dos argumento en la funcion, uno es el prototipo de la funcion y como segundo
 * argumento recibe el nombre de la clase que siempre debe ser una cadena
 */

export function Decorator(target: any, propertyName: string) {
	console.log('running decorator')
	target.className = target.constructor.name
	console.log(propertyName)
}

export function StaticDecorator(cls: Function, propertyName: string) {}

class Speaker {
	@Decorator
	num: number

	@StaticDecorator
	static fnParam: string
}

const speaker = new Speaker()
console.log((speaker as any).className)
