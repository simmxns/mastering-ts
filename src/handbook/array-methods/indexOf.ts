/**
 * Array.indexOf()
 * 
 * search a value into an array if this is found return the index if not -1
 */

export const indexOf: string[] = ['a', 'b', 'a', 'c', 'a', 'd']
const indexes: number[] = []
let idx: number = indexOf.indexOf('a')

while (idx != -1) {
	indexes.push(idx)
	idx = indexOf.indexOf('a', idx + 1) // [0, 2, 4]
}