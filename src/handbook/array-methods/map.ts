import content from '@/assets/contentdb.json'

/**
 * Array.map()
 * 
 * create new array from a function result apply in each value of the list
 */

export const map: Array<number> = [2, 4, 6, 8]

map.map(value => value * 2) // [4, 8, 12, 16]
content.quotes.map(value => console.log(value))

