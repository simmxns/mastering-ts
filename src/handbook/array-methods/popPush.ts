/**
 * Array.pop()
 * 
 * delete the last element of an array
 * it self returns the element removed
 */

export const pop = ['My', 'current', 'mood', 'is', '😎']
pop.pop() // '😎'
pop // [ 'My', 'current', 'mood', 'is' ]

/**
 * Array.push()
 * 
 * add a element at the end of an array
 * it self returns the element added
 */

export const push = pop.push('😓') // '😓'
push // ['My', 'current', 'mood', 'is', '😓']