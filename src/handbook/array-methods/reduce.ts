/**
 * Array.reduce()
 * 
 * Run a reductor function on each element of the list and returns it as a single value
 */

export const reduce = [1, 2, 3, 4, 5]

reduce.reduce((prev, curr) => prev + curr, 0) // 15