import { User, Tasks } from './db'

export function requestHandler(request: any, response: any): any {
	User.findById(request.userId, function (error, user) {
		if (error) {
			return response.send(error)
		} else {
			Tasks.findById(user.tasksId, function (error, tasks) {
				if (error) {
					return response.send(error)
				} else {
					tasks.completed = true
					tasks.save(function (error: any) {
						if (error) {
							return response.send(error)
						} else {
							response.send('Task completed!')
						}
					})
				}
			})
		}
	})
}