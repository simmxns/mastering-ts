import { User, Tasks } from './db'

export async function requestHandler(req: any, res: any) {
	try {
		const user = await User.findById(req.userId)
		const tasks = await Tasks.findById(user.tasksId)
		tasks.completed = true
		await tasks.save()
		res.send('Tasks completed!')
	} catch (e) {
		res.send(e) 
	}
}
