import { User, Tasks } from '../db'

/**
 * Promises
 * 
 * Las promesa utilizada para computación asíncrona, representan un valor que puede resolverse
 * inmediatamente, eventualmente en un futuro o nunca
 * 
 * Usar una promesa fuera de un async/await hará que nuestras promesas sean un poco más componibles
 * 
 * ```typescript
 * new Promise( executor, function(resolver, rechazar) { ... })
 * ```
 * 
 * Una promesa pendiente puede ser cumplida con un valor, o rechazada con una razón (error). Cuando
 * cualquiera de estas dos opciones sucede, los métodos asociados, encolados por el método then de
 * la promesa, son llamados. (Si la promesa ya ha sido cumplida o rechazada en el momento que es
 * anexado su correspondiente manejador, el manejador será llamado, de tal manera que no exista una
 * condición de carrera entre la operación asíncrona siendo completada y los manejadores siendo anexados).
 * 
 * NOTA: No se podrá utilizar el contenido de una promesa sin utilizar then
 * 
 * Se suele utilizar cuando las llamadas a datos no son instantaneos como llamadas a servidores o escrituras
 * en base de datos que sabemos que las repuestas no son inmediatas
 * 
 */
// first simple example
const promiseOne = Promise.resolve(1)
console.log(promiseOne)

promiseOne
	.then(x => x + 5) 
	.then(x => Promise.resolve(x + 5)) // both x are not the same (diffents clousures)
	.then(x => Promise.reject('Error something went wrong :('))
	.then(x => console.log('Unfortunately this dont gonna be called'))
	.catch(e => console.error(e))

// sample with delay to simulate a server response
export const delayed = (x: any) => new Promise<number>(( resolve, reject ) => {
	setTimeout(() => resolve(x), 900)
})

delayed(7)
	.then(x => {
		console.log(x)
		return delayed(x + 7)
	})
	.then(x => console.log(x))
	.then(x => Promise.reject('something went wrong :('))
	.catch(e => console.error(e))

export function requestHandler(req: any, res: any) {
	User.findById(req.userId)
		.then((user: any) => {
			return Tasks.findById(user.tasksId)
		})
		.then((tasks: any) => {
			tasks.completed = true
			tasks.save()
		})
		.then(() => {
			res.send('Tasks completed!')
		})
		.catch((errors: any) => {
			res.send(errors)
		})
}