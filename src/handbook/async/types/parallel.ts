import { taskOne, taskTwo } from './tasks'

/**
 * Parallel
 * 
 * All the promises runs at the sime time (in parallel to each other)
 */
async function main() { 
	try {
		console.time('Measuring time')
		const results = await Promise.all([taskOne(), taskTwo()])
		console.timeEnd('Measuring time')

		console.log("task one returns:", results[0])
		console.log("task two returns:", results[1])
	} catch (e) {
		console.error(e)
	}
}

main()