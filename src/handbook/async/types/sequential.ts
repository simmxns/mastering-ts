import { taskOne, taskTwo } from './tasks'

/**
 * Sequential
 * 
 * When one finish the other start, this is named sequential async
 */
async function main() { 
	try {
		console.time('Measuring time')
		const valueOne = taskOne()
		const valueTwo = taskTwo()
		console.timeEnd('Measuring time')

		console.log("task one returns:", valueOne)
		console.log("task two returns:", valueTwo)
	} catch (e) {
		console.error(e)
	}
}

main()